#Gra w życie

Program przeprowadzający symulację gry w życie.

Zasady gry:
 
 - Jeśli martwa komórka ma 3 sąsiadów to ożywa
 
 - Jeśli żywa komórka ma w następnym kroku 3 lub 2 sąsiadów to pozostaje żywa
 
 - Jeśli komórka ma w następnym kroku mniej niż 2 lub więcej niż 3 sąsiadów to umiera (kolejno z osamotnienia lub z przetłoczenia)
 
 Symulacja jest nieskończona, trzeba ją manualnie zakończyć przez wciśnięcie przycisku Stop.
 
 Warunki brzegowe - periodyczne.