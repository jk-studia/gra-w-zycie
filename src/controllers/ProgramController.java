package controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import model.Cell;
import model.ICell;


import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class ProgramController implements Initializable{

    @FXML
    private Button resetButton;
    @FXML
    private Button startButton;
    @FXML
    private TextField numberOfCellsField;
    @FXML
    private Button createButton;
    @FXML
    private Button randomizeButton;
    @FXML
    private TextField widthField;
    @FXML
    private TextField heightField;
    @FXML
    private Button stopButton;
    @FXML
    private ScrollPane scrollPane;

    private GraphicsContext gc;
    private Canvas canvas;
    private int width;
    private int height;
    private Cell cells[][];
    private Timeline timeline;
    private double simulationTimeStep = 1.0;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        width = 100;
        height = 100;
        createBoard();
        stopButton.setDisable(true);
    }

    private void createBoard(){
        cells = new Cell[width][height];
        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                cells[i][j] = new Cell(i, j);
            }
        }

        canvas = new Canvas(width*ICell.SIZE, height*ICell.SIZE);
        gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.DARKGREY);
        gc.fillRect(0,0, width*ICell.SIZE, height*ICell.SIZE);
        scrollPane.setContent(canvas);
    }

    private void draw(){
        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                gc.setFill(cells[i][j].getColor());
                int x = cells[i][j].getCoordX()*ICell.SIZE;
                int y = cells[i][j].getCoordY()*ICell.SIZE;
                gc.fillRect(x,y,ICell.SIZE,ICell.SIZE);
            }
        }
    }

    public void createButtonOnAction(ActionEvent actionEvent) {
        width = Integer.parseInt(widthField.getText());
        height = Integer.parseInt(heightField.getText());
        createBoard();
    }

    public void randomizeButtonOnAction(ActionEvent actionEvent) {
        int numberOFCells = Integer.parseInt(numberOfCellsField.getText());
        Random random = new Random();
        for(int i = 0; i < numberOFCells; i++){
            int x = random.nextInt(width);
            int y = random.nextInt(height);

            if(!cells[x][y].isAlive()){
                cells[x][y].revive();
            }
        }
        draw();
    }

    public void startButtonOnAction(ActionEvent actionEvent) {
        stopButton.setDisable(false);
        startButton.setDisable(true);
        randomizeButton.setDisable(true);
        createButton.setDisable(true);
        resetButton.setDisable(true);

        try {
            KeyFrame simulation = generateKeyFrameSimulation(simulationTimeStep);
            timeline = new Timeline(simulation);
            timeline.setCycleCount(Timeline.INDEFINITE);
            timeline.play();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private KeyFrame generateKeyFrameSimulation(double duration) {
        KeyFrame simulation = new KeyFrame(Duration.seconds(duration), e->{
            simulate();
            draw();
        });
        return simulation;
    }

    private void simulate(){
        Cell[][] tmp = new Cell[width][height];
        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                tmp[i][j] = cells[i][j].getCopy();
            }
        }


        for(int i = 0; i < width; i++){
            for(int j = 0; j < height; j++){
                int status = checkNeighbors(i,j,cells);
                if(status == 0){
                    tmp[i][j].kill();
                } else if(status == 1){
                    tmp[i][j].revive();
                }
            }
        }

        cells = tmp;
    }

    private int checkNeighbors(int x, int y, Cell temp[][]){
        int count = 0;
        for(int i = x-1; i < x+2; i++){
            for(int j = y-1; j < y+2; j++){
                if(i==x&&j==y)
                    continue;
                int coordx = i;
                int coordy = j;
                if(coordx<0)
                    coordx=width-1;
                if(coordy<0)
                    coordy=height-1;
                if(coordx>width-1)
                    coordx=0;
                if(coordy>height-1)
                    coordy=0;

                if(temp[coordx][coordy].isAlive())
                    count++;
            }
        }

        if(count == 3)
            return 1;
        if(count < 2 || count>3)
            return 0;
        return 2;
    }


    public void stopButtonOnAction(ActionEvent actionEvent) {
        timeline.stop();
        stopButton.setDisable(true);
        startButton.setDisable(false);
        randomizeButton.setDisable(false);
        createButton.setDisable(false);
        resetButton.setDisable(false);
    }

    public void resetButtonOnAction(ActionEvent actionEvent) {
        for(int i = 0; i < width; i++){
            for(int j = 0; j < width; j++){
                cells[i][j].kill();
            }
        }
        gc.setFill(Color.DARKGREY);
        gc.fillRect(0,0, width*ICell.SIZE, height*ICell.SIZE);
    }


}
