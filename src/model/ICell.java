package model;

public interface ICell {
    int SIZE = 5;
    void kill();
    void revive();
}
