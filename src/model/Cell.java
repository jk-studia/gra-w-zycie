package model;

import javafx.scene.paint.Color;

public class Cell implements ICell {

    private boolean alive;
    private Color color;
    private int coordX, coordY;

    private Cell(){
    }

    public Cell(int x, int y){
        alive = false;
        color = Color.DARKGREY;
        coordX = x;
        coordY = y;
    }

    @Override
    public void kill() {
        this.alive = false;
        color = Color.DARKGREY;
    }

    @Override
    public void revive() {
        this.alive = true;
        color = Color.RED;
    }


    public boolean isAlive() {
        return alive;
    }


    public int getCoordX() {
        return coordX;
    }

    private void setCoordX(int coordX){
        this.coordX = coordX;
    }

    private void setCoordY(int coordY){
        this.coordY = coordY;
    }

    public int getCoordY() {
        return coordY;
    }

    public Color getColor() {
        return color;
    }


    public Cell getCopy() {
        Cell cell = new Cell();
        cell.setAlive(this.alive);
        cell.setCoordX(this.coordX);
        cell.setCoordY(this.coordY);
        cell.setColor(this.color);
        return cell;
    }

    private void setColor(Color color) {
        this.color = color;
    }

    private void setAlive(boolean alive) {
        this.alive = alive;
    }
}
